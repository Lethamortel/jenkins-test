# Installation #

* wget -q -O - https://jenkins-ci.org/debian/jenkins-ci.org.key | sudo apt-key add -
* sudo sh -c 'echo deb http://pkg.jenkins-ci.org/debian binary/ > /etc/apt/sources.list.d/jenkins.list'
* sudo apt-get update
* sudo apt-get install jenkins


### Add SSH HOST ###

* Add the key to the id_rsa file
* Create file in ~/.ssh/config with:
	Host <name>
		Hostname <addr>
		User <user>
		IdentityFile <path/to/privatekey>
* Connect to it once to allow the connection
* Give chmod 600 on id_rsa if (too permisive error)
* Test it